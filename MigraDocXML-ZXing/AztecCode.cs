﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigraDocXML_ZXing
{
    public class AztecCode : Barcode
    {

        private ZXing.Aztec.AztecEncodingOptions _options;


        public AztecCode()
            : base()
        {
            _options = new ZXing.Aztec.AztecEncodingOptions();
            _writer.Options = _options;
            _writer.Format = ZXing.BarcodeFormat.AZTEC;
        }


        public int? ErrorCorrection { get => _options.ErrorCorrection; set => _options.ErrorCorrection = value; }

        public int? Layers { get => _options.Layers; set => _options.Layers = value; }

    }
}
