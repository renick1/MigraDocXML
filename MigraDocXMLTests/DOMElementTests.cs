﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
	public class DOMElementTests
	{
		[Fact]
		public void GetAllDescendentsCorrectCount()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<Table>" +
							"<Column Width=\"5cm\"/>" +
							"<Column Width=\"5cm\"/>" +
							"<Row C0=\"Hello\" C1=\"World\"/>" +
							"<Row C0=\"Howdy\" C1=\"Universe\"/>" +
						"</Table>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Table tbl = section.Children.OfType<Table>().First();
			Column col = tbl.Children.OfType<Column>().First();
			Row row = tbl.Children.OfType<Row>().First();
			Cell cell = row.Children.OfType<Cell>().First();

			Assert.Single(cell.GetAllDescendents());
			Assert.Equal(4, row.GetAllDescendents().Count);
			Assert.Equal(12, tbl.GetAllDescendents().Count);
			Assert.Equal(13, section.GetAllDescendents().Count);
			Assert.Equal(14, doc.GetAllDescendents().Count);
		}

		[Fact]
		public void GetAllDescendentsCorrectOrder()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<Table>" +
							"<Column Width=\"5cm\"/>" +
							"<Column Width=\"5cm\"/>" +
							"<Row C0=\"Hello\" C1=\"World\"/>" +
							"<Row C0=\"Howdy\" C1=\"Universe\"/>" +
						"</Table>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			var descendents = doc.GetAllDescendents();
			Assert.IsType<Section>(descendents[0]);
			Assert.IsType<Table>(descendents[1]);
			Assert.IsType<Column>(descendents[2]);
			Assert.IsType<Column>(descendents[3]);
			Assert.IsType<Row>(descendents[4]);
			Assert.IsType<C0>(descendents[5]);
			Assert.IsType<Paragraph>(descendents[6]);
			Assert.IsType<C1>(descendents[7]);
			Assert.IsType<Paragraph>(descendents[8]);
			Assert.IsType<Row>(descendents[9]);
			Assert.IsType<C0>(descendents[10]);
			Assert.IsType<Paragraph>(descendents[11]);
			Assert.IsType<C1>(descendents[12]);
			Assert.IsType<Paragraph>(descendents[13]);
		}
	}
}
