﻿using MigraDocXML;
using MigraDocXML.DOM;
using MigraDocXML.DOM.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class GraphicsTests
    {
        [Fact]
        public void GraphicsExpandedAfterRender()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Graphics>" +
                            "<Line Page=\"1\"/>" +
                        "</Graphics>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Graphics gfx = section.Children.OfType<Graphics>().First();

            Assert.Empty(gfx.Children);
            Assert.NotNull(gfx.ChildProcessor);

            gfx.ChildProcessor();

			Assert.Single(gfx.Children);
        }

		[Fact]
		public void ClosureCreated()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<Var i=\"7\"/>" +
						"<Graphics/>" +
						"<Set i=\"8\"/>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Graphics gfx = section.Children.OfType<Graphics>().First();
			
			//Make sure the Graphics element contains all of the document variables, all of the section variables, and it's own "Graphics" variable
			Assert.Equal(doc.GetOwnedVariables().Count() + section.GetOwnedVariables().Count() + 1, gfx.GetOwnedVariables().Count());
			foreach (var variable in doc.GetOwnedVariables())
				Assert.Contains(gfx.GetOwnedVariables(), x => x.Key == variable.Key);
			foreach (var variable in section.GetOwnedVariables())
				Assert.Contains(gfx.GetOwnedVariables(), x => x.Key == variable.Key);

			Assert.Equal(7, gfx.GetVariable("i"));
			Assert.Equal(8, section.GetVariable("i"));
		}
    }
}
