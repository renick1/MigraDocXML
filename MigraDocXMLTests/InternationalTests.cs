﻿using MigraDoc.DocumentObjectModel;
using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MigraDocXMLTests
{
	public class InternationalTests
	{
		[Fact]
		public void SetStringFromAttributeInIcelandic()
		{
			Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("is-IS");
			var testDOM = new TestDOM();
			PdfXmlReader.SetPropertyFromAttribute(testDOM, "StringProp", "123");
			Assert.Equal("123", testDOM.StringProp);
		}

		[Fact]
		public void SetNullDoubleFromAttributeInFrench()
		{
			Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("fr-FR");
			var testDOM = new TestDOM();
			PdfXmlReader.SetPropertyFromAttribute(testDOM, "NullDoubleProp", "123.0");
			Assert.Equal(123.0, testDOM.NullDoubleProp);
		}

		private class TestDOM : DOMElement
		{
			public int IntProp { get; set; }
			public int? NullIntProp { get; set; }

			public string StringProp { get; set; }

			public double DoubleProp { get; set; }
			public double? NullDoubleProp { get; set; }

			public override DocumentObject GetModel() => throw new NotImplementedException();
		}
	}
}
