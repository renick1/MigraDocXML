﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class TextFrameTests
    {
        [Fact]
        public void SimpleTextFrame()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<TextFrame>" +
                            "<p>Test</p>" +
                        "</TextFrame>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            TextFrame frame = section.Children.OfType<TextFrame>().First();
            Paragraph p = frame.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.Single(p.GetParagraphModel().Elements);

            Assert.Single(section.GetSectionModel().Elements);
            Assert.Equal(section.GetSectionModel(), frame.GetTextFrameModel().Section);
        }
    }
}
