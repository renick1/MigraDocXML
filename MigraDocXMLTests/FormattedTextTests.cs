﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class FormattedTextTests
    {
        [Fact]
        public void BoldTest()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<p>This is <b>bold</b></p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Paragraph p = section.Children.OfType<Paragraph>().First();

            var pElements = p.GetParagraphModel().Elements;
            Assert.Equal(2, pElements.Count);
            Assert.Equal("This is ", pElements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);

            Assert.True(pElements.OfType<MigraDoc.DocumentObjectModel.FormattedText>().First().Bold);
            Assert.Equal("bold", pElements
                .OfType<MigraDoc.DocumentObjectModel.FormattedText>().First()
                .Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First()
                .Content
            );
        }
    }
}
