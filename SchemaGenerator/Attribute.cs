﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemaGenerator
{
    public abstract class AttributeBase
    {
    }


    public class Attribute : AttributeBase
    {
        public string Name { get; private set; }

        public string Type { get; private set; }

        public string Use { get; private set; }

        public string Default { get; private set; }

        public string Documentation { get; private set; }

        public Attribute(string name, string type, string use = null, string defaultArg = null, string documentation = null)
        {
            Name = name;
            Type = type;
            Use = use;
            Default = defaultArg;
            Documentation = documentation;
        }
    }


    public class AnyAttribute : AttributeBase
    {
        public string ProcessContents { get; private set; }

        public AnyAttribute(string processContents)
        {
            ProcessContents = processContents;
        }
    }
}
