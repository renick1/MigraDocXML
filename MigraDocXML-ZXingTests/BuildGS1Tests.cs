﻿using MigraDocXML_ZXing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MigraDocXML_ZXingTests
{
	public class BuildGS1Tests
	{
		[Fact]
		public void SingleAI()
		{
			var input = new List<KeyValuePair<string, string>>()
			{
				new KeyValuePair<string, string>("11", "123456")
			};
			var fnc1 = (char)29;
			var output = GS1.BuildGS1Barcode(input, fnc1);
			Assert.Equal(fnc1 + "11123456", output);
		}
		[Fact]
		public void MultipleFixedLengthAIs()
		{
			var input = new List<KeyValuePair<string, string>>()
			{
				new KeyValuePair<string, string>("11", "123456"),
				new KeyValuePair<string, string>("20", "AB")
			};
			var fnc1 = (char)29;
			var output = GS1.BuildGS1Barcode(input, fnc1);
			Assert.Equal(fnc1 + "1112345620AB", output);
		}

		[Fact]
		public void FixedThenVariableLengthAIs()
		{
			var input = new List<KeyValuePair<string, string>>()
			{
				new KeyValuePair<string, string>("11", "123456"),
				new KeyValuePair<string, string>("10", "12345")
			};
			var fnc1 = (char)29;
			var output = GS1.BuildGS1Barcode(input, fnc1);
			Assert.Equal(fnc1 + "111234561012345", output);
		}

		[Fact]
		public void VariableThenFixedLengthAIs()
		{
			var input = new List<KeyValuePair<string, string>>()
			{
				new KeyValuePair<string, string>("10", "12345"),
				new KeyValuePair<string, string>("11", "123456")
			};
			var fnc1 = (char)29;
			var output = GS1.BuildGS1Barcode(input, fnc1);
			Assert.Equal(fnc1 + "1012345" + fnc1 + "11123456", output);
		}

		[Fact]
		public void InvalidAICode()
		{
			var input = new List<KeyValuePair<string, string>>()
			{
				new KeyValuePair<string, string>("03", "123456")
			};
			var fnc1 = (char)29;
			Assert.Throws<Exception>(() => GS1.BuildGS1Barcode(input, fnc1));
		}

		[Fact]
		public void InvalidDataLength()
		{
			var input = new List<KeyValuePair<string, string>>()
			{
				new KeyValuePair<string, string>("01", "123456")
			};
			var fnc1 = (char)29;
			Assert.Throws<Exception>(() => GS1.BuildGS1Barcode(input, fnc1));
		}
	}
}
