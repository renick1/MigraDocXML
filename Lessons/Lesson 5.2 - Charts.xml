<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Style Target="Hyperlink">
        <Setters Font.Color="Blue" Font.Underline="Single"/>
    </Style>

    <Style Target="Chart">
        <Setters Width="8cm" Height="8cm"/>
    </Style>

    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 5.2 - Charts</p>

        <p>MigraDoc offers support for generating charts, which is now also supported through MigraDocXML. Since the charting objects are quite complex, allowing for a lot of configurability in how your charts are put together, rather than talking through the numerous options, we'll instead adapt some examples taken from here <a>https://github.com/DavidS/MigraDoc/blob/master/PDFsharp/dev/PdfSharp.Charting.Demo/
        PdfSharp.Charting.Demo/ChartSamples.cs</a> and recreate them using MigraDocXML.</p>

        <p Style="SubTitle">Line Chart</p>

        <p Style="Code">
            &lt;Chart Type="Line"&gt;
            {Space(4)}&lt;Series Name="Series 1" Values="[1, 5, -3, 20, 11]"/&gt;
            {Space(4)}&lt;Series Name="Series 2" Values="[22, 4, 12, 8, 12]"/&gt;
            {Space(4)}&lt;Series Name="Series 3" Values="[12, 14, -3, 18, 1]"/&gt;
            {Space(4)}&lt;XAxis MajorTickMark="Outside" Title.Caption="X-Axis"/&gt;
            {Space(4)}&lt;YAxis MajorTickMark="Outside" Title.Caption="Y-Axis" HasMajorGridlines="true"/&gt;
            {Space(4)}&lt;PlotArea LineFormat.Color="DarkGray" LineFormat.Width="1"/&gt;
            {Space(4)}&lt;BottomArea&gt;
            {Space(8)}&lt;Legend/&gt;
            {Space(4)}&lt;/BottomArea&gt;
            {Space(4)}&lt;XSeries Values="['A', 'B', 'C', 'D', 'E', 'F']"/&gt;
            &lt;/Chart&gt;
            
        </p>
        <p/>

        <Chart Type="Line">
            <Series Name="Series 1" Values="[1, 5, -3, 20, 11]"/>
            <Series Name="Series 2" Values="[22, 4, 12, 8, 12]"/>
            <Series Name="Series 3" Values="[12, 14, -3, 18, 1]"/>
            <XAxis MajorTickMark="Outside" Title.Caption="X-Axis"/>
            <YAxis MajorTickMark="Outside" Title.Caption="Y-Axis" HasMajorGridlines="true"/>
            <PlotArea LineFormat.Color="DarkGray" LineFormat.Width="1"/>
            <BottomArea>
                <Legend/>
            </BottomArea>
            <XSeries Values="['A', 'B', 'C', 'D', 'E', 'F']"/>
        </Chart>

        <p>The <b>Series</b> &amp; <b>XSeries</b> values are passed in as an array from the scripting language. These can either be as hard-coded values as in the above example, or from some array within your model data</p>

        
        
        <PageBreak/>

        <p Style="SubTitle">Column Chart</p>

        <p Style="Code">
            &lt;Chart Type="Column2D" Width="14cm"&gt;
            {Space(4)}&lt;Series Name="Series 1" Values="[1, 5, -3, 20, 11]"/&gt;
            {Space(4)}&lt;Series Name="Series 2" Values="[22, 4, 12, 8, 12]"/&gt;
            {Space(4)}&lt;Series Name="Series 3" Values="[12, 14, -3, 18, 1]"/&gt;
            {Space(4)}&lt;Series Name="Series 4" Values="[17, 13, 10, 9, 15]"/&gt;
            {Space(4)}&lt;XAxis TickLabels.Format="00" MajorTickMark="Outside" Title.Caption="X-Axis"/&gt;
            {Space(4)}&lt;YAxis MajorTickMark="Outside" HasMajorGridlines="true"/&gt;
            {Space(4)}&lt;PlotArea LineFormat.Color="DarkGray" LineFormat.Width="1"/&gt;
            {Space(4)}&lt;RightArea&gt;
            {Space(8)}&lt;Legend/&gt;
            {Space(4)}&lt;/RightArea&gt;
            {Space(4)}&lt;DataLabel Type="Value" Position="OutsideEnd"/&gt;
            &lt;/Chart&gt;
            
        </p>
        <p/>

        <Chart Type="Column2D" Width="14cm">
            <Series Name="Series 1" Values="[1, 5, -3, 20, 11]"/>
            <Series Name="Series 2" Values="[22, 4, 12, 8, 12]"/>
            <Series Name="Series 3" Values="[12, 14, -3, 18, 1]"/>
            <Series Name="Series 4" Values="[17, 13, 10, 9, 15]"/>
            <XAxis TickLabels.Format="00" MajorTickMark="Outside" Title.Caption="X-Axis"/>
            <YAxis MajorTickMark="Outside" HasMajorGridlines="true"/>
            <PlotArea LineFormat.Color="DarkGray" LineFormat.Width="1"/>
            <RightArea>
                <Legend/>
            </RightArea>
            <DataLabel Type="Value" Position="OutsideEnd"/>
        </Chart>

        
        
        <PageBreak/>

        <p Style="SubTitle">Column Stacked Chart</p>

        <p Style="Code">
            &lt;Chart Type="ColumnStacked2D" Width="10cm"&gt;
            {Space(4)}&lt;Series Name="Series 1" Values="[1, 5, -3, 20, 11]"/&gt;
            {Space(4)}&lt;Series Name="Series 2" Values="[22, 4, 12, 8, 12]"/&gt;
            {Space(4)}&lt;Series Name="Series 3" Values="[12, 14, -3, 18, 1]"/&gt;
            {Space(4)}&lt;Series Name="Series 4" Values="[17, 13, 10, 9, 15]"/&gt;
            {Space(4)}&lt;XAxis TickLabels.Format="00" MajorTickMark="Outside" Title.Caption="X-Axis"/&gt;
            {Space(4)}&lt;YAxis MajorTickMark="Outside" HasMajorGridlines="true"/&gt;
            {Space(4)}&lt;PlotArea LineFormat.Color="DarkGray" LineFormat.Width="1"/&gt;
            {Space(4)}&lt;RightArea&gt;
            {Space(8)}&lt;Legend/&gt;
            {Space(4)}&lt;/RightArea&gt;
            {Space(4)}&lt;DataLabel Type="Value" Position="OutsideEnd"/&gt;
            &lt;/Chart&gt;

        </p>
        <p/>

        <Chart Type="ColumnStacked2D" Width="10cm">
            <Series Name="Series 1" Values="[1, 5, -3, 20, 11]"/>
            <Series Name="Series 2" Values="[22, 4, 12, 8, 12]"/>
            <Series Name="Series 3" Values="[12, 14, -3, 18, 1]"/>
            <Series Name="Series 4" Values="[17, 13, 10, 9, 15]"/>
            <XAxis TickLabels.Format="00" MajorTickMark="Outside" Title.Caption="X-Axis"/>
            <YAxis MajorTickMark="Outside" HasMajorGridlines="true"/>
            <PlotArea LineFormat.Color="DarkGray" LineFormat.Width="1"/>
            <RightArea>
                <Legend/>
            </RightArea>
            <DataLabel Type="Value" Position="OutsideEnd"/>
        </Chart>



        <PageBreak/>

        <p Style="SubTitle">Bar Chart</p>

        <p Style="Code">
            &lt;Chart Type="Bar2D" Width="14cm" Height="10cm"&gt;
            {Space(4)}&lt;Series Name="Series 1" Values="[1, 5, -3, 20, 11]"/&gt;
            {Space(4)}&lt;Series Name="Series 2" Values="[22, 4, 12, 8, 12]"/&gt;
            {Space(4)}&lt;Series Name="Series 3" Values="[12, 14, -3, 18, 1]"/&gt;
            {Space(4)}&lt;Series Name="Series 4" Values="[17, 13, 10, 9, 15]"/&gt;
            {Space(4)}&lt;XAxis MajorTickMark="Outside" Title.Caption="X-Axis"/&gt;
            {Space(4)}&lt;YAxis MajorTickMark="Outside" HasMajorGridlines="true"/&gt;
            {Space(4)}&lt;PlotArea LineFormat.Color="DarkGray" LineFormat.Width="1"/&gt;
            {Space(4)}&lt;RightArea&gt;
            {Space(8)}&lt;Legend/&gt;
            {Space(4)}&lt;/RightArea&gt;
            {Space(4)}&lt;DataLabel Type="Value" Position="InsideEnd"/&gt;
            &lt;/Chart&gt;
            
        </p>
        <p/>

        <Chart Type="Bar2D" Width="14cm" Height="10cm">
            <Series Name="Series 1" Values="[1, 5, -3, 20, 11]"/>
            <Series Name="Series 2" Values="[22, 4, 12, 8, 12]"/>
            <Series Name="Series 3" Values="[12, 14, -3, 18, 1]"/>
            <Series Name="Series 4" Values="[17, 13, 10, 9, 15]"/>
            <XAxis MajorTickMark="Outside" Title.Caption="X-Axis"/>
            <YAxis MajorTickMark="Outside" HasMajorGridlines="true"/>
            <PlotArea LineFormat.Color="DarkGray" LineFormat.Width="1"/>
            <RightArea>
                <Legend/>
            </RightArea>
            <DataLabel Type="Value" Position="InsideEnd"/>
        </Chart>



        <PageBreak/>

        <p Style="SubTitle">Bar Stacked Chart</p>

        <p Style="Code">
            &lt;Chart Type="BarStacked2D" Width="14cm"&gt;
            {Space(4)}&lt;Series Name="Series 1" Values="[1, 5, -3, 20, 11]"/&gt;
            {Space(4)}&lt;Series Name="Series 2" Values="[22, 4, 12, 8, 12]"/&gt;
            {Space(4)}&lt;Series Name="Series 3" Values="[12, 14, -3, 18, 1]"/&gt;
            {Space(4)}&lt;Series Name="Series 4" Values="[17, 13, 10, 9, 15]"/&gt;
            {Space(4)}&lt;XAxis MajorTickMark="Outside" Title.Caption="X-Axis"/&gt;
            {Space(4)}&lt;YAxis MajorTickMark="Outside" HasMajorGridlines="true"/&gt;
            {Space(4)}&lt;PlotArea LineFormat.Color="DarkGray" LineFormat.Width="1"/&gt;
            {Space(4)}&lt;RightArea&gt;
            {Space(8)}&lt;Legend/&gt;
            {Space(4)}&lt;/RightArea&gt;
            {Space(4)}&lt;DataLabel Type="Value" Position="Center"/&gt;
            &lt;/Chart&gt;

        </p>
        <p/>

        <Chart Type="BarStacked2D" Width="14cm">
            <Series Name="Series 1" Values="[1, 5, -3, 20, 11]"/>
            <Series Name="Series 2" Values="[22, 4, 12, 8, 12]"/>
            <Series Name="Series 3" Values="[12, 14, -3, 18, 1]"/>
            <Series Name="Series 4" Values="[17, 13, 10, 9, 15]"/>
            <XAxis MajorTickMark="Outside" Title.Caption="X-Axis"/>
            <YAxis MajorTickMark="Outside" HasMajorGridlines="true"/>
            <PlotArea LineFormat.Color="DarkGray" LineFormat.Width="1"/>
            <RightArea>
                <Legend/>
            </RightArea>
            <DataLabel Type="Value" Position="Center"/>
        </Chart>



        <PageBreak/>

        <p Style="SubTitle">Area Chart</p>

        <p Style="Code">
            &lt;Chart Type="Area2D"&gt;
            {Space(4)}&lt;Series Values="[31, 9, 15, 28, 13]"/&gt;
            {Space(4)}&lt;Series Values="[22, 7, 12, 21, 12]"/&gt;
            {Space(4)}&lt;Series Values="[16, 5, 3, 20, 11]"/&gt;
            {Space(4)}&lt;XAxis TickLabels.Format="00" MajorTickMark="Outside" Title.Caption="X-Axis"/&gt;
            {Space(4)}&lt;YAxis MajorTickMark="Outside" HasMajorGridlines="true"/&gt;
            {Space(4)}&lt;PlotArea LineFormat.Color="DarkGray" LineFormat.Width="1"/&gt;
            {Space(4)}&lt;TopArea&gt;
            {Space(8)}&lt;Legend/&gt;
            {Space(4)}&lt;/TopArea&gt;
            &lt;/Chart&gt;

        </p>
        <p/>

        <Chart Type="Area2D">
            <Series Values="[31, 9, 15, 28, 13]"/>
            <Series Values="[22, 7, 12, 21, 12]"/>
            <Series Values="[16, 5, 3, 20, 11]"/>
            <XAxis TickLabels.Format="00" MajorTickMark="Outside" Title.Caption="X-Axis"/>
            <YAxis MajorTickMark="Outside" HasMajorGridlines="true"/>
            <PlotArea LineFormat.Color="DarkGray" LineFormat.Width="1"/>
            <TopArea>
                <Legend/>
            </TopArea>
        </Chart>



        <PageBreak/>

        <p Style="SubTitle">Pie Chart</p>

        <p Style="Code">
            &lt;Chart Type="Pie2D" Width="10cm"&gt;
            {Space(4)}&lt;Series Values="[1, 5, 11, -3, 20]"/&gt;
            {Space(4)}&lt;XSeries Values="['Production', 'Lab', 'Licenses', 'Taxes', 'Insurances']"/&gt;
            {Space(4)}&lt;RightArea&gt;
            {Space(8)}&lt;Legend/&gt;
            {Space(4)}&lt;/RightArea&gt;
            {Space(4)}&lt;DataLabel Type="Percent" Position="OutsideEnd"/&gt;
            &lt;/Chart&gt;
        </p>
        <p/>

        <Chart Type="Pie2D" Width="10cm">
            <Series Values="[1, 5, 11, -3, 20]"/>
            <XSeries Values="['Production', 'Lab', 'Licenses', 'Taxes', 'Insurances']"/>
            <RightArea>
                <Legend/>
            </RightArea>
            <DataLabel Type="Percent" Position="OutsideEnd"/>
        </Chart>



        <PageBreak/>

        <p Style="SubTitle">Exploded Pie Chart</p>

        <p Style="Code">
            &lt;Chart Type="PieExploded2D" Width="10cm"&gt;
            {Space(4)}&lt;Series Values="[1, 17, 45, 5, 3, 20, 11, 23, 8, 19, 34, 56, 23, 45]"/&gt;
            {Space(4)}&lt;LeftArea&gt;
            {Space(8)}&lt;Legend/&gt;
            {Space(4)}&lt;/LeftArea&gt;
            {Space(4)}&lt;DataLabel Type="Percent" Position="Center"/&gt;
            &lt;/Chart&gt;
        </p>
        <p/>

        <Chart Type="PieExploded2D" Width="10cm">
            <Series Values="[1, 17, 45, 5, 3, 20, 11, 23, 8, 19, 34, 56, 23, 45]"/>
            <LeftArea>
                <Legend/>
            </LeftArea>
            <DataLabel Type="Percent" Position="Center"/>
        </Chart>



        <PageBreak/>

        <p Style="SubTitle">Combination Chart</p>

        <p Style="Code">
            &lt;Chart Width="10cm"&gt;
            {Space(4)}&lt;Series ChartType="Column2D" HasDataLabel="true" Values="[1, 17, 45, 5, 3, 20, 11, 23, 8, 19]"/&gt;
            {Space(4)}&lt;Series ChartType="Line" Values="[41, 7, 5, 45, 13, 10, 21, 13, 18, 9]"/&gt;
            {Space(4)}&lt;XSeries Values="['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N']"/&gt;
            {Space(4)}&lt;XAxis MajorTickMark="Outside" Title.Caption="X-Axis"/&gt;
            {Space(4)}&lt;YAxis MajorTickMark="Outside" HasMajorGridlines="true"/&gt;
            {Space(4)}&lt;PlotArea LineFormat.Color="DarkGray" LineFormat.Width="1"/&gt;
            {Space(4)}&lt;LeftArea&gt;
            {Space(8)}&lt;Legend LineFormat.Visible="true"/&gt;
            {Space(4)}&lt;/LeftArea&gt;
            &lt;/Chart&gt;
        </p>
        <p/>

        <Chart Width="10cm">
            <Series ChartType="Column2D" HasDataLabel="true" Values="[1, 17, 45, 5, 3, 20, 11, 23, 8, 19]"/>
            <Series ChartType="Line" Values="[41, 7, 5, 45, 13, 10, 21, 13, 18, 9]"/>
            <XSeries Values="['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N']"/>
            <XAxis MajorTickMark="Outside" Title.Caption="X-Axis"/>
            <YAxis MajorTickMark="Outside" HasMajorGridlines="true"/>
            <PlotArea LineFormat.Color="DarkGray" LineFormat.Width="1"/>
            <LeftArea>
                <Legend LineFormat.Visible="true"/>
            </LeftArea>
        </Chart>
    </Section>
</Document>