<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Style Target="FormattedText" Name="Code">
        <Setters Font.Name="Consolas" Font.Size="8" Font.Bold="false" Font.Italic="false"/>
    </Style>
    
    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 3.0 - Introduction To Scripts</p>

        <Var contentWidth="Section.PageSetup.ContentWidth"/>
        
        <p>In the lessons so far you've learnt about the various display objects that act as building blocks for putting together a document. However, the documents you're capable of producing are all entirely static in nature, giving no ability to display or react to data from outside sources.</p>

        <p>In the next set of lessons we'll look at how to inject data into your designs from data models, perform calculations, and make use of MigraDocXML's logical objects to dynamically change our designs.</p>

        
        
        <p Style="SubTitle">Curly Braces</p>

        <p>MigraDocXML is really made up of 2 separate languages. The first is the XML-based one we've already looked at for defining the layout of content on a page. The second is a very lightweight scripting language called EvalScript which is largely inspired in syntax by C#, though far far smaller in its ambitions.</p>

        <p>Scripts are added as single expression snippets, where each snippet <b>must</b> be able to be evaluated to return a value. This is very important, <b>scripts are only capable of getting data, they are not capable of setting data</b>. This is done as a security feature, since as we'll see in future lessons, you can inject objects from your program into a PDF template.</p>
        <p>Curly braces are our way of entering the scripting language, running an expression, and outputting the result as text into our document. For example:</p>

        <p Style="Code">&lt;p&gt;```{3 + 5}```&lt;/p&gt;</p>

        <p>Gets interpretted as:</p>

        <p Style="Code">&lt;p&gt;8&lt;/p&gt;</p>

        <p>Which produces:</p>

        <p>{3 + 5}</p>

        <p>Scripts can be added as part of the content of an element (as seen above) or as part of the value of an attribute, for example:</p>

        <p Style="Code">&lt;p Format.SpaceBefore=&quot;```{1 + 2}```mm&quot;&gt;Test&lt;/p&gt;</p>

        <p>Which gets evaluated to:</p>

        <p Style="Code">&lt;p Format.SpaceBefore=&quot;{1 + 2}mm&quot;&gt;Test&lt;/p&gt;</p>
        
        
        
        <p Style="SubTitle">Literals</p>

        <p>In programming, a literal is a way of representing a fixed value. In the above examples, 1, 2, 3 &amp; 5 are all numeric literals. EvalScript defines a number of different literals that can be expressed.</p>

        <list Type="BulletList1" NumberPosition="5mm">
            <p><i Style="Code">true</i> &amp; <i Style="Code">false</i> are boolean literals.</p>

            <p>Any text surrounded by single quotes, for example <i Style="Code">'hello'</i>, is a string literal. To include a single-quote inside a string literal, the single-quote must be escaped with a backslash. For example <i Style="Code">'I\'m James'</i> returns <i Style="Code">'I'm James'</i>.</p>

            <p><i Style="Code">null</i> is the null literal. This signifies a complete lack of a value.</p>

            <p>Any number is automatically identified as an int if it's a whole number, or a double otherwise. You can use suffixes to explicitly define the type of a numeric literal, for example 57.12D is a double, 57.12F is a float, 57.12M is a decimal, and 57L is a long. If all of this means nothing to you, these are standard .NET numeric types. All you really need to know is that 3 or 4 get stored as whole numbers, whereas 2.0 or 3.14 get stored with decimal places.</p>
        </list>



        <PageBreak/>

        <p Style="SubTitle">Operators</p>

        <p>We've already seen a demonstration of the addition operator to add two numbers together, but there are whole load more available to you.</p>

        <Table Borders.Color="Gray">
            <Column Width="{contentWidth * 0.1}" Format.Alignment="Center"/>
            <Column Width="{contentWidth * 0.25}" Format.Alignment="Center"/>
            <Column Width="{contentWidth * 0.2}" Format.Alignment="Center"/>
            <Column Width="{contentWidth * 0.45}"/>
            
            <Row Heading="true" Format.Font.Bold="true" C0="Operator" C1="Examples" C2="Results" C3="Description"/>

            <Row C0="+" C3="When used on numbers, this sums them together. If either value being added is a string, the result is returned as a string.">
                <C1>
                    <p>```{1 + 2}```</p>
                    <p>```{3.5 + 6.1}```</p>
                    <p>```{'Hello ' + 'world'}```</p>
                    <p>```{99 + ' Red Balloons'}```</p>
                </C1>
                <C2>
                    <p>{1 + 2}</p>
                    <p>{3.5 + 6.1}</p>
                    <p>{'Hello ' + 'world'}</p>
                    <p>{99 + ' Red Balloons'}</p>
                </C2>
            </Row>

            <Row C0="-" C3="Subtraction">
                <C1>
                    <p>```{5 - 2}```</p>
                    <p>```{4.9 - 6.5}```</p>
                </C1>
                <C2>
                    <p>{5 - 2}</p>
                    <p>{4.9 - 6.5}</p>
                </C2>
            </Row>

            <Row C0="*" C3="Multiplication">
                <C1>
                    <p>```{4 * 3}```</p>
                    <p>```{1.1 * 6}```</p>
                </C1>
                <C2>
                    <p>{4 * 3}</p>
                    <p>{1.1 * 6}</p>
                </C2>
            </Row>

            <Row C0="/" C3="Division">
                <C1>
                    <p>```{15 / 3}```</p>
                    <p>```{19 / 5}```</p>
                    <p>```{19.0 / 5}```</p>
                </C1>
                <C2>
                    <p>{15 / 3}</p>
                    <p>{19 / 5}</p>
                    <p>{19.0 / 5}</p>
                </C2>
            </Row>

            <Row C0="^" C3="Power operator, this raises the number to the left by the number to the right.">
                <C1>
                    <p>```{5 ^ 2}```</p>
                    <p>```{9 ^ 0.5}```</p>
                </C1>
                <C2>
                    <p>{5 ^ 2}</p>
                    <p>{9 ^ 0.5}</p>
                </C2>
            </Row>

            <Row C0="%" C3="Modulus operator, this gives the remainder after the number to the left is divided by the number to the right. 7 = (2 * 3) + 1, hence 7 % 3 = 1">
                <C1>
                    <p>```{10 % 7}```</p>
                    <p>```{9 % 3}```</p>
                    <p>```{-10 % 7}```</p>
                </C1>
                <C2>
                    <p>{10 % 7}</p>
                    <p>{9 % 3}</p>
                    <p>{-10 % 7}</p>
                </C2>
            </Row>

            <Row C0="= or ==" C3="Equality operator, this checks that the value on the left is equal to the value on the right. The single and double equality symbols both act exactly the same.">
                <C1>
                    <p>```{3 = 3}```</p>
                    <p>```{4 == 5}```</p>
                    <p>```{'hello' = 'hello'}```</p>
                </C1>
                <C2>
                    <p>{3 = 3}</p>
                    <p>{4 == 5}</p>
                    <p>{'hello' = 'hello'}</p>
                </C2>
            </Row>

            <Row C0="!=" C3="Not equal operator, this tests if two values aren't equal">
                <C1>
                    <p>```{3 != 3}```</p>
                    <p>```{4 != 5}```</p>
                    <p>```{'hello' != 'hello'}```</p>
                </C1>
                <C2>
                    <p>{3 != 3}</p>
                    <p>{4 != 5}</p>
                    <p>{'hello' != 'hello'}</p>
                </C2>
            </Row>

            <Row C0="&lt;{NewLine()}&amp;lt;{NewLine()}LT" C3="Less than operator, tests if the value on the left is less than the value on the right">
                <C1>
                    <p>```{3 &lt; 4}```</p>
                    <p>```{4 &amp;lt; 3}```</p>
                    <p>```{4 LT 4}```</p>
                </C1>
                <C2>
                    <p>{3 &lt; 4}</p>
                    <p>{4 &lt; 3}</p>
                    <p>{4 LT 4}</p>
                </C2>
            </Row>
            
            <Row C0="&lt;={NewLine()}&amp;lt;={NewLine()}LT=" C3="Less than or equal operator, tests if the value on the left is less than or equal to the value on the right">
                <C1>
                    <p>```{3 &lt;= 4}```</p>
                    <p>```{4 &lt;= 3}```</p>
                    <p>```{4 LT= 4}```</p>
                </C1>
                <C2>
                    <p>{3 &lt;= 4}</p>
                    <p>{4 &lt;= 3}</p>
                    <p>{4 LT= 4}</p>
                </C2>
            </Row>

            <Row C0="&gt;{NewLine()}&amp;gt;{NewLine()}GT" C3="Greater than operator, tests if the value on the left is greater than the value on the right">
                <C1>
                    <p>```{3 &gt; 4}```</p>
                    <p>```{4 &gt; 3}```</p>
                    <p>```{4 GT 4}```</p>
                </C1>
                <C2>
                    <p>{3 > 4}</p>
                    <p>{4 > 3}</p>
                    <p>{4 GT 4}</p>
                </C2>
            </Row>
            
            <Row C0="&gt;={NewLine()}&amp;gt;={NewLine()}GT=" C3="Greater than or equal operator, tests if the value on the left is greater than or equal to the value on the right">
                <C1>
                    <p>```{3 &gt;= 4}```</p>
                    <p>```{4 &gt;= 3}```</p>
                    <p>```{4 GT= 4}```</p>
                </C1>
                <C2>
                    <p>{3 >= 4}</p>
                    <p>{4 >= 3}</p>
                    <p>{4 GT= 4}</p>
                </C2>
            </Row>

            <Row C0="AND{NewLine()}&amp;&amp;" C3="AND operator for testing if multiple conditions are all true">
                <C1>
                    <p>```{2 &lt; 3 AND 3 &lt; 4}```</p>
                    <p>```{5 &lt; 3 AND 3 &lt; 4}```</p>
                    <p>```{2 &lt; 3 &amp;&amp; 3 == 4}```</p>
                </C1>
                <C2>
                    <p>{2 &lt; 3 AND 3 &lt; 4}</p>
                    <p>{5 &lt; 3 AND 3 &lt; 4}</p>
                    <p>{2 &lt; 3 &amp;&amp; 3 == 4}</p>
                </C2>
            </Row>

            <Row C0="OR{NewLine()}||" C3="OR operator for testing if any one of multiple conditions are true">
                <C1>
                    <p>```{2 &lt; 3 OR 3 &lt; 4}```</p>
                    <p>```{5 &lt; 3 || 3 &lt; 4}```</p>
                    <p>```{2 &gt; 3 OR 3 == 4}```</p>
                </C1>
                <C2>
                    <p>{2 &lt; 3 OR 3 &lt; 4}</p>
                    <p>{5 &lt; 3 || 3 &lt; 4}</p>
                    <p>{2 &gt; 3 OR 3 == 4}</p>
                </C2>
            </Row>
        </Table>

        <p>A minor annoyance of working with XML is that the <b>&lt;</b> and <b>&gt;</b> symbols are reserved, which presents a problem if we're writing expressions that rely on the less than or greater than operators. As demonstrated above, MigraDocXML allows for comparisons to be written as either <b>```{3 &lt; 4}```</b>, <b>```{3 &amp;lt; 4}```</b> or <b>```{3 LT 4}```</b>. The first is easiest to read, but your XML editor may complain, whereas the others don't break XML syntax rules, but are slightly harder to read. It comes down to personal preference which method you use.</p>
        
        
        
        <p Style="SubTitle">Functions</p>
        
        <p>EvalScript also supports function calls, either to one of the functions that come built in, or optionally, if working with .NET data, to functions defined on the objects (this requires the script to be run using special permissions which will be covered in lesson 3.7).</p>
        
        <p>The built-in EvalScript functions can be called simply using the function name, followed by opening and closing brackets, with any function parameters listed inside the brackets, separated by commas: <i Style="Code">Contains('hill', 'hi')</i>. Alternatively, the first parameter can precede the function (for example, <i Style="Code">'hill'.Contains('hi')</i>), this often produces code that reads far more easily and allows greater support for chaining functions one after another. There are several later lessons devoted to listing all of the built-in EvalScript functions.</p>

        
        
        <p Style="SubTitle">Escaping Code</p>

        <p>If you want to display or store some script as text within a document, much like I've been doing in my examples, you can wrap your script with 3 backticks before and after like this:</p>

        <p Style="Code">&lt;p&gt;{Replace('` ` `', ' ', '')}```{2 * 7}```{Replace('` ` `', ' ', '')}&lt;/p&gt;</p>

        <p>This will print:</p>

        <p>```{2 * 7}```</p>

    </Section>
</Document>