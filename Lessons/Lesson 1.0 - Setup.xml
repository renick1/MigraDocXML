<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 1.0 - Setup</p>

        <p>Welcome to this series of lessons on MigraDocXML, an open source markup language for designing PDF documents, built on top of the popular MigraDoc libraries.</p>

        <p>All of these lessons have actually been written using MigraDocXML, so if you want to know how to do anything you see in these documents, or just want to look around some real world examples, the MigraDocXML markup files are all available in the same folder as the PDF lesson files they were used to generate.</p>

        <p>MigraDocXML adds many benefits to working with MigraDoc, including:</p>

        <list Type="BulletList1">
            <p>Designs aren't contained within assembled code, making changes far easier</p>
            <p>A greatly extended styling system</p>
            <p>A layout document that's far easier to read, especially for designers familiar with HTML or XAML</p>
            <p>Easy support for working with data from JSON, CSV &amp; XML files, as well as directly from .NET objects</p>
        </list>

        
        
        <p Style="SubTitle">Choosing which version to use</p>

        <p>To include MigraDocXML into your project, the easiest way to do it is through Nuget. There are 2 versions of the package available, &quot;MigraDocXML&quot; and &quot;MigraDocXML-WPF&quot;.</p>

        <p>The 2 versions of MigraDocXML reflect the 2 main versions of MigraDoc itself, that differ in the rendering technology they use. MigraDocXML uses GDI rendering, while MigraDocXML-WPF uses WPF (Windows Presentation Framework) for its rendering. Which version you use will depend largely on whether you're already referencing the WPF files in your project or not. For example, if you're developing a WinForms or Console application, you almost certainly want to use the GDI version.</p>



        <p Style="SubTitle">XML Setup</p>

        <p>The first thing you want to do before you start designing documents is to make sure you have access to the MigraDocXML schema. This will allow your XML editor to provide code hints &amp; validation as you're designing and will generally allow you to be far more productive.</p>

        <p>
            The MigraDocXML schema can be found here: <a Font.Underline="Single" Font.Color="Blue">https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd</a>
        </p>

        <p>The simplest way to reference the schema is just to use the web location:</p>

        <p Style="Code">
            &lt;?xml version="1.0" encoding="utf-8"?&gt;
            &lt;Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            {Space(10)}xsi:noNamespaceSchemaLocation=
            {Space(14)}"https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd"&gt;
            &lt;/Document&gt;

        </p>

        <p>Though if you wish, you can also download it to your computer and reference it locally.</p>



        <p Style="SubTitle">Choosing a Text Editor</p>

        <p>Of the free options available, I would strongly recommend Visual Studio as having by far the best XML editor of everything I've tried so far. If you already have it, that's brilliant, you should be all good to go! If you don't, getting just the parts you need doesn't need to involve any massive downloads. Just download the latest version of the Visual Studio installer, from there you'll have several tabs with options of what you want installed. Make sure all Workloads are unticked and in the Individual components tab, tick just <b>Class Designer</b> under Code tools. This barebones install will give you everything you need to get started.</p>

        <p>If you really don't want to, or aren't able to install Visual Studio, Notepad++ with the XML Tools plugin can also work quite well.</p>

        <PageBreak/>
        
        
        
        <p Style="SubTitle">Running the Code</p>

        <p>Now you've got yourself set up for designing PDFs in MigraDocXML, all we need now is a little bit of code to run the processes that convert the xml data into an actual PDF document.</p>

        <p Style="Code">
            using MigraDocXML;
            
            ...
            
            <FormattedText Font.Color="Green">//Define where to pick up the xml layout file from, and where to save the output pdf file to</FormattedText>
            string xmlFile = @"C:\Users\james\Documents\Test.xml";
            string pdfFile = @"C:\Users\james\Documents\Test.pdf";
            
            <FormattedText Font.Color="Green">//Process the xml file contents to generate a display object model for the PDF</FormattedText>
            var pdfDoc = new PdfXmlReader(xmlFile).Run();
            <FormattedText Font.Color="Green">//More on the reason for this in lesson 3.4 - Loops &amp; Jumps</FormattedText>
            if(pdfDoc == null)
                {Space(4)}return;
            <FormattedText Font.Color="Green">//Pass the generated document object model into the PDF renderer and run it</FormattedText>
            var renderer = new MigraDoc.Rendering.PdfDocumentRenderer() {LeftBrace()} Document = pdfDoc.GetDocumentModel() {RightBrace()};
            renderer.RenderDocument();
            <FormattedText Font.Color="Green">//Save the generated PDF</FormattedText>
            renderer.PdfDocument.Save(pdfFile);
            <FormattedText Font.Color="Green">//Use the default PDF viewer to display the document</FormattedText>
            System.Diagnostics.Process.Start(pdfFile);
            
        </p>

        <p>You're now all set up to start working with MigraDocXML.</p>

    </Section>
</Document>