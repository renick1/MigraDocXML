<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Style Target="FormattedText" Name="Code">
        <Setters Font.Name="Consolas" Font.Size="8" Font.Bold="false" Font.Italic="false"/>
    </Style>
    
    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 3.5 - Injecting CSV Data</p>

        <p>This is the first of 4 lessons devoted to injecting data into your documents, and we're starting off simple with the humble CSV file. I would recommend reading this lesson with the accompanying CSV file.</p>

        <p>First off, there is a minor code change needed since we need to tell MigraDocXML where to get the CSV data from. On the PdfXmlReader object, all you need to do is call <b>RunWithCsvFile(path to file)</b> instead of <b>Run()</b>.</p>

        <p>Any data that's to be included in your document automatically comes through on a variable called <b>Model</b>. In the case of CSV data, this model is a list of records, where we can loop through each one and access the record data, like so:</p>

        <p Style="Code">
            &lt;Table Borders.Color=&quot;Black&quot;&gt;
            {Space(4)}&lt;Column Width=&quot;5cm&quot;/&gt;
            {Space(4)}&lt;Column Width=&quot;5cm&quot;/&gt;
            {Space(4)}&lt;Column Width=&quot;5cm&quot;/&gt;

            {Space(4)}&lt;Row Heading=&quot;true&quot; Format.Font.Bold=&quot;true&quot; C0=&quot;Forename&quot; C1=&quot;Surname&quot; C2=&quot;Job Title&quot;/&gt;

            {Space(4)}&lt;ForEach Var=&quot;employee&quot; In=&quot;Model&quot;&gt;
            {Space(8)}&lt;Row C0=&quot;```{employee.Forename}```&quot; C1=&quot;```{employee.Surname}```&quot; C2=&quot;```{employee.Job}```&quot;/&gt;
            {Space(4)}&lt;/ForEach&gt;
            &lt;/Table&gt;
        </p>

        <p>Produces:</p>

        <Table Borders.Color="Black">
            <Column Width="5cm"/>
            <Column Width="5cm"/>
            <Column Width="5cm"/>

            <Row Heading="true" Format.Font.Bold="true" C0="Forename" C1="Surname" C2="Job Title"/>

            <ForEach Var="employee" In="Model">
                <Row C0="{employee.Forename}" C1="{employee.Surname}" C2="{employee.Job}"/>
            </ForEach>
        </Table>

        <p>The first row of the CSV file is read as a headings line, with the data of each record stored against the heading names.</p>
        
    </Section>
</Document>